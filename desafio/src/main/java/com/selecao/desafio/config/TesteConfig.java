package com.selecao.desafio.config;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.selecao.desafio.entities.DadosCombustivelCSV;
import com.selecao.desafio.entities.DataColeta;
import com.selecao.desafio.entities.Distribuidora;
import com.selecao.desafio.entities.HistoricoPrecoCombustivel;
import com.selecao.desafio.entities.Usuario;
import com.selecao.desafio.repositories.DadosCombustivelCSVRepository;
import com.selecao.desafio.repositories.DataColetaRepository;
import com.selecao.desafio.repositories.DistribuidoraRepository;
import com.selecao.desafio.repositories.HistoricoPrecoCombustivelRepository;
import com.selecao.desafio.repositories.UsuarioRepository;

@Configuration
@Profile("test")
public class TesteConfig implements CommandLineRunner {

	@Autowired
	private DadosCombustivelCSVRepository dadosCombustivelCSVRepository;
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private HistoricoPrecoCombustivelRepository historicoRepository;
	
	@Autowired
	private DistribuidoraRepository distribuidoraRepository;

	@Autowired
	private DataColetaRepository dataColetaRepository;
	
	@Override
	public void run(String... args) throws Exception {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String data1 = "25/01/2020";
		String data2 = "20/01/2020";
		
		Usuario u1 = new Usuario(null, "Saulo", "Alves", "sauloalves@gmail.com", sdf.parse(data1));
		Usuario u2 = new Usuario(null, "Teste", "Usuario", "teste@email.com", sdf.parse(data2));
		usuarioRepository.saveAll(Arrays.asList(u1, u2));
		
		HistoricoPrecoCombustivel h1 = new HistoricoPrecoCombustivel(null, "DIESEL", sdf.parse(data2), 4.19);
		HistoricoPrecoCombustivel h2 = new HistoricoPrecoCombustivel(null, "DIESEL", sdf.parse(data1), 4.19);
		historicoRepository.saveAll(Arrays.asList(h1, h2));
		
		Distribuidora d1 = new Distribuidora(null, "DISTRIBUIDORA TESTE 1");
		Distribuidora d2 = new Distribuidora(null, "DISTRIBUIDORA TESTE 2");
		distribuidoraRepository.saveAll(Arrays.asList(d1,d2));
		
		DataColeta dc1 = new DataColeta(null, sdf.parse(data1));
		DataColeta dc2 = new DataColeta(null, sdf.parse(data1));
		dataColetaRepository.saveAll(Arrays.asList(dc1, dc2));
		
		DadosCombustivelCSV dcCSV1 = new DadosCombustivelCSV(null, "CO", "DF", "BRASILIA", d1, 
				7890, "DIESEL", dc1, 0.0, 3.699, "R$ / litro", "IPIRANGA");
		
		DadosCombustivelCSV dcCSV2 = new DadosCombustivelCSV(null, "CO", "DF", "BRASILIA", d2, 
				7970, "DIESEL", dc2, 3.218, 3.559, "R$ / litro", "PETROBRAS DISTRIBUIDORA S.A.");
		
		dadosCombustivelCSVRepository.saveAll(Arrays.asList(dcCSV1, dcCSV2));
	
	}
	
	
}
