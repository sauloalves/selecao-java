package com.selecao.desafio.dtos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.selecao.desafio.entities.DadosCombustivelCSV;

public class DadosCombustivelCSVDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String siglaRegiao;
	private String siglaEstado;	
	private String municipio;
	private String distribuidora;
	private Integer codInstalacao;
	private String produto;
	private String dataColeta;
	private Double valorDeCompra;
	private Double valorDeVenda;
	private String unidadeDeMedida;
	private String bandeira;
	
	public DadosCombustivelCSVDTO() {
		
	}

	public DadosCombustivelCSVDTO(DadosCombustivelCSV dados) {
		super();
		this.id = dados.getId();
		this.siglaRegiao = dados.getSiglaRegiao();
		this.siglaEstado = dados.getSiglaEstado();
		this.municipio = dados.getMunicipio();
		this.distribuidora = dados.getDistribuidora().getNome();
		this.codInstalacao = dados.getCodInstalacao();
		this.produto = dados.getProduto();
		this.dataColeta = dataFormatoDDMMYYYY(dados.getDataColeta().getData());
		this.valorDeCompra = dados.getValorDeCompra();
		this.valorDeVenda = dados.getValorDeVenda();
		this.unidadeDeMedida = dados.getUnidadeDeMedida();
		this.bandeira = dados.getBandeira();
	}

	public Long getId() {
		return id;
	}

	public String getSiglaRegiao() {
		return siglaRegiao;
	}

	public void setSiglaRegiao(String siglaRegiao) {
		this.siglaRegiao = siglaRegiao;
	}

	public String getSiglaEstado() {
		return siglaEstado;
	}

	public void setSiglaEstado(String siglaEstado) {
		this.siglaEstado = siglaEstado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getRevendedora() {
		return distribuidora;
	}

	public void setRevendedora(String distribuidora) {
		this.distribuidora = distribuidora;
	}

	public Integer getCodInstalacao() {
		return codInstalacao;
	}

	public void setCodInstalacao(Integer codInstalacao) {
		this.codInstalacao = codInstalacao;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public String getDataColeta() {
		return dataColeta;
	}

	public void setDataColeta(String dataColeta) {
		this.dataColeta = dataColeta;
	}

	public Double getValorDeCompra() {
		return valorDeCompra;
	}

	public void setValorDeCompra(Double valorDeCompra) {
		this.valorDeCompra = valorDeCompra;
	}

	public Double getValorDeVenda() {
		return valorDeVenda;
	}

	public void setValorDeVenda(Double valorDeVenda) {
		this.valorDeVenda = valorDeVenda;
	}

	public String getUnidadeDeMedida() {
		return unidadeDeMedida;
	}

	public void setUnidadeDeMedida(String unidadeDeMedida) {
		this.unidadeDeMedida = unidadeDeMedida;
	}

	public String getBandeira() {
		return bandeira;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}
	
	private String dataFormatoDDMMYYYY(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(data);
	}
}
