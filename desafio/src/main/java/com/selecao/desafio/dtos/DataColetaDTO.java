package com.selecao.desafio.dtos;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.selecao.desafio.entities.DadosCombustivelCSV;
import com.selecao.desafio.entities.DataColeta;

public class DataColetaDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String data;

	private List<DadosCombustivelCSV> dadosCombustivel = new ArrayList<>();
	
	public DataColetaDTO() {
		
	}

	public DataColetaDTO(DataColeta dc) {
		super();
		this.id = dc.getId();
		this.data = dataFormatoDDMMYYYY(dc.getData());
		this.dadosCombustivel = dc.getDadosCombustivel();
	}

	public Long getId() {
		return id;
	}

	public List<DadosCombustivelCSV> getDadosCombustivel() {
		return dadosCombustivel;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	private String dataFormatoDDMMYYYY(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(data);
	}
}
