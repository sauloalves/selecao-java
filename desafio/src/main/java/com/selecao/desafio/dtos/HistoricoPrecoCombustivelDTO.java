package com.selecao.desafio.dtos;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.selecao.desafio.entities.HistoricoPrecoCombustivel;

public class HistoricoPrecoCombustivelDTO {
	
	private Long id;
	private String combustivel;
	private String data;
	private Double preco;
	
	public HistoricoPrecoCombustivelDTO() {
		
	}

	public HistoricoPrecoCombustivelDTO(HistoricoPrecoCombustivel hpc) {
		super();
		this.id = hpc.getId();
		this.combustivel = hpc.getCombustivel();
		this.data = dataFormatoDDMMYYYY(hpc.getData());
		this.preco = hpc.getPreco();
	}

	public Long getId() {
		return id;
	}

	public String getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	private String dataFormatoDDMMYYYY(Date data) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(data);
	}

}
