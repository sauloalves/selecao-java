package com.selecao.desafio.dtos;

import java.io.Serializable;

public class MediasCompraVendaPorAtributoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String valorAtributo;
	private double mediaValorCompra;
	private double mediaValorVenda;
	
	public MediasCompraVendaPorAtributoDTO() {
		
	}
	
	public MediasCompraVendaPorAtributoDTO(String atributo, double mediaValorCompra, double mediaValorVenda) {
		super();
		this.valorAtributo = atributo;
		this.mediaValorCompra = mediaValorCompra;
		this.mediaValorVenda = mediaValorVenda;
	}

	public String getValorAtributo() {
		return valorAtributo;
	}

	public void setValorAtributo(String atributo) {
		this.valorAtributo = atributo;
	}

	public double getMediaValorCompra() {
		return mediaValorCompra;
	}

	public void setMediaValorCompra(double mediaValorCompra) {
		this.mediaValorCompra = mediaValorCompra;
	}

	public double getMediaValorVenda() {
		return mediaValorVenda;
	}

	public void setMediaValorVenda(double mediaValorVenda) {
		this.mediaValorVenda = mediaValorVenda;
	}
	
}
