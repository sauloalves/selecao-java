package com.selecao.desafio.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class DadosCombustivelCSV implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String siglaRegiao;
	private String siglaEstado;	
	private String municipio;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="distribuidora_id")
	private Distribuidora distribuidora;
	private Integer codInstalacao;
	private String produto;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="dataColeta_id")
	private DataColeta dataColeta;
	private Double valorDeCompra;
	private Double valorDeVenda;
	private String unidadeDeMedida;
	private String bandeira;
	
	public DadosCombustivelCSV() {
		
	}

	public DadosCombustivelCSV(Long id, String siglaRegiao, String siglaEstado, String municipio, Distribuidora distribuidora,
			Integer codInstalacao, String produto, DataColeta dataColeta, Double valorDeCompra, Double valorDeVenda,
			String unidadeDeMedida, String bandeira) {
		super();
		this.id = id;
		this.siglaRegiao = siglaRegiao;
		this.siglaEstado = siglaEstado;
		this.municipio = municipio;
		this.distribuidora = distribuidora;
		this.codInstalacao = codInstalacao;
		this.produto = produto;
		this.dataColeta = dataColeta;
		this.valorDeCompra = valorDeCompra;
		this.valorDeVenda = valorDeVenda;
		this.unidadeDeMedida = unidadeDeMedida;
		this.bandeira = bandeira;
	}

	public Long getId() {
		return id;
	}

	public String getSiglaRegiao() {
		return siglaRegiao;
	}

	public void setSiglaRegiao(String siglaRegiao) {
		this.siglaRegiao = siglaRegiao;
	}

	public String getSiglaEstado() {
		return siglaEstado;
	}

	public void setSiglaEstado(String siglaEstado) {
		this.siglaEstado = siglaEstado;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public Distribuidora getDistribuidora() {
		return distribuidora;
	}

	public void setDistribuidora(Distribuidora distribuidora) {
		this.distribuidora = distribuidora;
	}

	public Integer getCodInstalacao() {
		return codInstalacao;
	}

	public void setCodInstalacao(Integer codInstalacao) {
		this.codInstalacao = codInstalacao;
	}

	public String getProduto() {
		return produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public DataColeta getDataColeta() {
		return dataColeta;
	}

	public void setDataColeta(DataColeta dataColeta) {
		this.dataColeta = dataColeta;
	}

	public Double getValorDeCompra() {
		return valorDeCompra;
	}

	public void setValorDeCompra(Double valorDeCompra) {
		this.valorDeCompra = valorDeCompra;
	}

	public Double getValorDeVenda() {
		return valorDeVenda;
	}

	public void setValorDeVenda(Double valorDeVenda) {
		this.valorDeVenda = valorDeVenda;
	}

	public String getUnidadeDeMedida() {
		return unidadeDeMedida;
	}

	public void setUnidadeDeMedida(String unidadeDeMedida) {
		this.unidadeDeMedida = unidadeDeMedida;
	}

	public String getBandeira() {
		return bandeira;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DadosCombustivelCSV other = (DadosCombustivelCSV) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
