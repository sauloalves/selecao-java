package com.selecao.desafio.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.selecao.desafio.dtos.MediasCompraVendaPorAtributoDTO;
import com.selecao.desafio.entities.DadosCombustivelCSV;
import com.selecao.desafio.entities.DataColeta;
import com.selecao.desafio.entities.Distribuidora;

public interface DadosCombustivelCSVRepository extends JpaRepository<DadosCombustivelCSV, Long> {
	
	@Query("select distinct dc.municipio from DadosCombustivelCSV dc")
	List<String> getMunicipios();

	@Query("select avg(dc.valorDeVenda) from DadosCombustivelCSV dc where upper(dc.municipio) = upper(?1)")
	float mediaDePrecoPorMunicipio(String municipio);

	@Query("select distinct dc.siglaRegiao from DadosCombustivelCSV dc")
	List<String> getSiglasRegiao();

	@Query("select dc from DadosCombustivelCSV dc where dc.siglaRegiao = ?1")
	List<DadosCombustivelCSV> getDadosPorSiglaRegiao(String sigla);

	@Query("select d from Distribuidora d")
	List<Distribuidora> agrupaPorDistribuidora();

	@Query("select dc from DataColeta dc")
	List<DataColeta> agrupaPorDataDaColeta();
	
	@Query("select new com.selecao.desafio.dtos.MediasCompraVendaPorAtributoDTO( dc.bandeira, avg(dc.valorDeCompra), avg(dc.valorDeVenda) ) from DadosCombustivelCSV dc group by dc.bandeira")
	List<MediasCompraVendaPorAtributoDTO> valorMedioCompraVendaBandeira();

	@Query("select new com.selecao.desafio.dtos.MediasCompraVendaPorAtributoDTO( dc.municipio, avg(dc.valorDeCompra), avg(dc.valorDeVenda) ) from DadosCombustivelCSV dc group by dc.municipio")
	List<MediasCompraVendaPorAtributoDTO> valorMedioCompraVendaMunicipio();

}
