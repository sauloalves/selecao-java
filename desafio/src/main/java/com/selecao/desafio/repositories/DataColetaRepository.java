package com.selecao.desafio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.selecao.desafio.entities.DataColeta;

public interface DataColetaRepository extends JpaRepository<DataColeta, Long> {
	
}
