package com.selecao.desafio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.selecao.desafio.entities.Distribuidora;

public interface DistribuidoraRepository extends JpaRepository<Distribuidora, Long> {
	
}
