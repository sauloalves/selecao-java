package com.selecao.desafio.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.selecao.desafio.entities.HistoricoPrecoCombustivel;

public interface HistoricoPrecoCombustivelRepository extends JpaRepository<HistoricoPrecoCombustivel, Long> {

	@Query("select h from HistoricoPrecoCombustivel h order by h.data desc")
	List<HistoricoPrecoCombustivel> getHistorico();
	
}
