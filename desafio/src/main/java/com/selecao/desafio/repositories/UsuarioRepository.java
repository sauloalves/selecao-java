package com.selecao.desafio.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.selecao.desafio.entities.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
}
