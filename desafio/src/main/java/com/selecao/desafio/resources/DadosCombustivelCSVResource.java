package com.selecao.desafio.resources;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.selecao.desafio.dtos.DadosCombustivelCSVDTO;
import com.selecao.desafio.dtos.DataColetaDTO;
import com.selecao.desafio.dtos.MediasCompraVendaPorAtributoDTO;
import com.selecao.desafio.entities.DadosCombustivelCSV;
import com.selecao.desafio.entities.DataColeta;
import com.selecao.desafio.entities.Distribuidora;
import com.selecao.desafio.services.DadosCombustivelCSVService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(description="Recursos para consulta de dados da comercialização de combustíveis importados do CSV.")
@RestController
@RequestMapping(value = "/csv")
public class DadosCombustivelCSVResource {
	
	@Autowired
	private DadosCombustivelCSVService service;
	
	
	@ApiOperation(value = "Importa CSV com dados relativo a combustiveis.", notes = "Caso o nome do arquivo "
			+ "informado não seja válido o programa executará com o arquivo defaultCSV.csv que está na pasta raiz.")
	@PostMapping
	public ResponseEntity <Void> importaCSV(
			@ApiParam(value = "Informe o nome do arquivo no corpo da requisição. Ex: NomeDoArquivo.csv (sem aspas)."
					+ " Este arquivo deve estar no diretório raiz do projeto.") 
			@RequestBody String nomeDoArquivo) {
		service.lerArquivoCSV(nomeDoArquivo);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Retorna uma lista de municípios que realizou operações com combutíveis.")
	@GetMapping(value = "listaMunicipios")
	public ResponseEntity<List<String>> getMunicipios() {
		return ResponseEntity.ok().body(service.getMunicipios());
	}
	
	@ApiOperation(value = "Retorna a média de preço de combustível com base no nome do município.")
	@GetMapping(value = "mediaDePrecoPorMunicipio/{municipio}")
	public ResponseEntity<Float> mediaDePrecoPorMunicipio(
			
			@ApiParam(value="Nome do município. Exemplo: 'BRASILIA'. Para obter mais municípios consulte"
					+ " o recurso listaMunicipios.", example = "BRASILIA", required = true)
			
			@PathVariable String municipio) {
		return ResponseEntity.ok().body(service.mediaDePrecoPorMunicipio(municipio));
	}
	
	@ApiOperation(value = "Retorna as siglas das regiões em que houve operações com combustíveis.")
	@GetMapping(value = "listaSiglasRegiao")
	public ResponseEntity<List<String>> getSiglasRegiao() {
		return ResponseEntity.ok().body(service.getSiglasRegiao());
	}
	
	@ApiOperation(value = "Retorne todas as informações importadas por sigla da região.")
	@GetMapping(value = "dadosPorSiglaRegiao/{sigla}")
	public ResponseEntity<List<DadosCombustivelCSVDTO>> dadosPorSiglaRegiao(
			
			@ApiParam(value="Sigla da região. Exemplo: 'CO' (Centro-Oeste). Para obter mais siglas consulte "
					+ "o recurso listaSiglasRegiao.", example = "CO", required = true) 
			
			@PathVariable String sigla ) {
		
		List<DadosCombustivelCSV> dcList = service.dadosPorSiglaRegiao(sigla);
		List<DadosCombustivelCSVDTO> listDto = dcList.stream()
				.map(dadosCombustivel -> new DadosCombustivelCSVDTO(dadosCombustivel))
				.collect(Collectors.toList());

		return ResponseEntity.ok().body(listDto);
	}
	
	@ApiOperation(value = "Retorna os dados agrupados por distribuidora.")
	@GetMapping(value = "agrupaPorDistribuidora")
	public ResponseEntity<List<Distribuidora>> agrupaPorDistribuidora() {
		return ResponseEntity.ok().body(service.agrupaPorDistribuidora());
	}
	
	@ApiOperation(value = "Retorna os dados agrupados pela data da coleta.")
	@GetMapping(value = "agrupaPorDataDaColeta")
	public ResponseEntity<List<DataColetaDTO>> agrupaPorDataDaColeta() {
		List<DataColeta> list = service.agrupaPorDataDaColeta();
		List<DataColetaDTO> listDto = list.stream().map(dataColeta -> new DataColetaDTO(dataColeta))
				.collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@ApiOperation(value = "Retorne o valor médio do valor da compra e do valor da venda por bandeira.")
	@GetMapping(value = "valorMedioCompraVendaBandeira")
	public ResponseEntity<List<MediasCompraVendaPorAtributoDTO>> valorMedioCompraVendaBandeira() {
		return ResponseEntity.ok().body(service.valorMedioCompraVendaBandeira());
	}
	
	@ApiOperation(value = "Retorna o valor médio do valor da compra e do valor da venda por município.")
	@GetMapping(value = "valorMedioCompraVendaMunicipio")
	public ResponseEntity<List<MediasCompraVendaPorAtributoDTO>> valorMedioCompraVendaMunicipio() {
		return ResponseEntity.ok().body(service.valorMedioCompraVendaMunicipio());
	}
}
