package com.selecao.desafio.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.selecao.desafio.dtos.HistoricoPrecoCombustivelDTO;
import com.selecao.desafio.entities.HistoricoPrecoCombustivel;
import com.selecao.desafio.services.HistoricoPrecoCombustivelService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(description = "Recursos para CRUD de histórico de preço de combustíveis")
@RestController
@RequestMapping(value = "/historicos")
public class HistoricoPrecoCombustivelResource {
	
	@Autowired
	private HistoricoPrecoCombustivelService service;

	@ApiOperation(value = "Retorna uma lista contendo o histórico de preço")
	@GetMapping
	public ResponseEntity<List<HistoricoPrecoCombustivelDTO>> findAll() {
		List<HistoricoPrecoCombustivel> list = service.findAll();
		List<HistoricoPrecoCombustivelDTO> listDto = list.stream().map(hpc -> new HistoricoPrecoCombustivelDTO(hpc))
				.collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@ApiOperation(value = "Retorna o hitórico de preços ordenados do mais recente ao mais antigo")
	@GetMapping(value = "porData")
	public ResponseEntity<List<HistoricoPrecoCombustivelDTO>> getHistorico() {
		List<HistoricoPrecoCombustivel> list = service.historico();
		List<HistoricoPrecoCombustivelDTO> listDto = list.stream().map(hpc -> new HistoricoPrecoCombustivelDTO(hpc))
				.collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@ApiOperation(value = "Retorna um dado específico do histórico")
	@GetMapping(value = "/{id}")
	public ResponseEntity<HistoricoPrecoCombustivelDTO> findById(
			@ApiParam(value="Número inteiro que represente o id de um dado do histórico.", example = "1", required = true)
			@PathVariable Long id){
		
		HistoricoPrecoCombustivelDTO obj = new HistoricoPrecoCombustivelDTO(service.findById(id));	
		return ResponseEntity.ok().body(obj);
	}
	
	@ApiOperation(value = "Insere um novo dado em histórico de preços")
	@PostMapping
	public ResponseEntity<HistoricoPrecoCombustivelDTO> insert(
			@ApiParam(value="Objeto json com dados para o histórico de preço de combustíveis.", required = true)
			@RequestBody HistoricoPrecoCombustivelDTO obj) {
		
		obj = new HistoricoPrecoCombustivelDTO(service.gravaHistorico(obj));
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").
				buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).body(obj);
	}
	
	@ApiOperation(value = "Exclui um dado do histórico de preço")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity <Void> delete(
			@ApiParam(value="Número inteiro que represente o id de um dado do histórico.", example = "1", required = true)
			@PathVariable Long id) {
		
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Altera um dado específico do histórico de preço")
	@PutMapping(value = "/{id}")
	public ResponseEntity<HistoricoPrecoCombustivelDTO> update(
			@ApiParam(value="Número inteiro que represente o id de um dado do histórico.", example = "1", required = true)
			@PathVariable Long id, 
			@ApiParam(value="Objeto json com dados para o histórico de preço de combustíveis.", required = true)
			@RequestBody HistoricoPrecoCombustivelDTO objHpc) {
		
		HistoricoPrecoCombustivel obj = service.update(id, objHpc);
		HistoricoPrecoCombustivelDTO objDto = new HistoricoPrecoCombustivelDTO(obj);
		return ResponseEntity.ok().body(objDto);
	}
}
