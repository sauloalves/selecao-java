package com.selecao.desafio.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.selecao.desafio.dtos.UsuarioDTO;
import com.selecao.desafio.entities.Usuario;
import com.selecao.desafio.services.UsuarioService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api( description = "Recursos para CRUD de usuário")
@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioResource {
	
	@Autowired
	private UsuarioService service;

	@ApiOperation(value = "Retorna uma lista com todos os usuários cadastrados")
	@GetMapping
	public ResponseEntity<List<UsuarioDTO>> findAll() {
		List<Usuario> list = service.findAll();
		List<UsuarioDTO> listDto = list.stream().map(usuario -> new UsuarioDTO(usuario)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@ApiOperation(value = "Retorna um determinado usuário especificado pelo id")
	@GetMapping(value = "/{id}")
	public ResponseEntity<UsuarioDTO> findById(
			@ApiParam(value="Número inteiro que represente o id do usuário.", example = "1", required = true) 
			@PathVariable Long id) {
		
		UsuarioDTO obj = new UsuarioDTO(service.findById(id));
		return ResponseEntity.ok().body(obj);
	}
	
	@ApiOperation(value = "Cadastra um novo usuário")
	@PostMapping
	public ResponseEntity<UsuarioDTO> insert(
			@ApiParam(value="Objeto json com dados do usuário. Obs: O atributo data segue o formato dd/mm/aaaa", required = true)
			@RequestBody UsuarioDTO usuario) {
		
		usuario = new UsuarioDTO(service.gravaUsuario(usuario));
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").
				buildAndExpand(usuario.getId()).toUri();
		return ResponseEntity.created(uri).body(usuario);
	}
	
	@ApiOperation(value = "Exclui um determinado usuário especificado pelo id")
	@DeleteMapping(value = "/{id}")
	public ResponseEntity <Void> delete(
			@ApiParam(value="Número inteiro que represente o id do usuário.", example = "1", required = true)
			@PathVariable Long id) {
		
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Atualiza os dados de um determinado usuário")
	@PutMapping(value = "/{id}")
	public ResponseEntity<UsuarioDTO> update(
			@ApiParam(value="Número inteiro que represente o id do usuário.", example = "1", required = true)
			@PathVariable Long id, 
			@ApiParam(value="Objeto json com dados do usuário.", required = true)
			@RequestBody UsuarioDTO usuario) {
		
		usuario = new UsuarioDTO(service.update(id, usuario));
		return ResponseEntity.ok().body(usuario);
	}
}
