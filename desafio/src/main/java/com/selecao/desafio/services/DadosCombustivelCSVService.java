package com.selecao.desafio.services;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecao.desafio.dtos.MediasCompraVendaPorAtributoDTO;
import com.selecao.desafio.entities.DadosCombustivelCSV;
import com.selecao.desafio.entities.DataColeta;
import com.selecao.desafio.entities.Distribuidora;
import com.selecao.desafio.repositories.DadosCombustivelCSVRepository;

@Service
public class DadosCombustivelCSVService {
	
	@Autowired
	private DadosCombustivelCSVRepository repository;
	
	@Autowired
	private DistribuidoraService distribuidoraService;
	
	@Autowired
	private DataColetaService dataColetaService;
	
	public void lerArquivoCSV(String path) {

		try(BufferedReader br = new BufferedReader( new InputStreamReader(new FileInputStream(path), "UTF-8") )){
			String line = br.readLine();
			int numLine = 1;
			while(line != null) {
				if (numLine > 1)
					carregaBaseDeDados(line, numLine);
				line = br.readLine();
				numLine++;
			}
		} catch(IOException e) {
			System.out.println("lerArquivoCSV - Error: " + e.getMessage());
			lerArquivoCSV("defaultCSV.csv");
		}
	}
	
	public void carregaBaseDeDados(String line, int numLine) {
		String [] contentCSV = line.split("  ");
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			if(contentCSV[7].equals(""))
				contentCSV[7] = "0";
			else
				contentCSV[7] = contentCSV[7].replace(',', '.');
			
			if(contentCSV[8].equals(""))
				contentCSV[8] = "0";
			else
				contentCSV[8] = contentCSV[8].replace(',', '.');
			
			Distribuidora distribuidora = new Distribuidora(null, contentCSV[3]);
			distribuidora = distribuidoraService.insert(distribuidora);
			
			DataColeta data = new DataColeta(null, sdf.parse(contentCSV[6]));
			data = dataColetaService.insert(data);
			
			DadosCombustivelCSV obj = new DadosCombustivelCSV(null, contentCSV[0], contentCSV[1], contentCSV[2], distribuidora, 
					Integer.parseInt(contentCSV[4]), contentCSV[5], data, Double.parseDouble(contentCSV[7]), 
					Double.parseDouble(contentCSV[8]), contentCSV[9], contentCSV[10]);
			
			repository.save(obj);

			
		}catch(ParseException e) {
			System.out.println("carregaBaseDeDados -PE Error:" + e.getMessage() + "linha: " + numLine);
		} catch(NumberFormatException e) {
			System.out.println("carregaBaseDeDados -NFE Error:" + e.getMessage() + "linha: " + numLine);
		}
		
	}

	public List<String> getMunicipios() {
		return repository.getMunicipios();
	}

	public float mediaDePrecoPorMunicipio(String municipio) {
		return repository.mediaDePrecoPorMunicipio(municipio);		
	}

	public List<String> getSiglasRegiao() {
		return repository.getSiglasRegiao();
	}

	public List<DadosCombustivelCSV> dadosPorSiglaRegiao(String sigla) {
		return repository.getDadosPorSiglaRegiao(sigla);
	}
	
	public List<Distribuidora> agrupaPorDistribuidora() {
		return repository.agrupaPorDistribuidora();
	}
	
	public List<DataColeta> agrupaPorDataDaColeta() {
		return repository.agrupaPorDataDaColeta();
	}
	
	public List<MediasCompraVendaPorAtributoDTO> valorMedioCompraVendaBandeira() {
		return repository.valorMedioCompraVendaBandeira();
	}
	
	public List<MediasCompraVendaPorAtributoDTO> valorMedioCompraVendaMunicipio() {
		return repository.valorMedioCompraVendaMunicipio();
	}

}
