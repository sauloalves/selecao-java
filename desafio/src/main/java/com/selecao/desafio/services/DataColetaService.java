package com.selecao.desafio.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecao.desafio.entities.DataColeta;
import com.selecao.desafio.repositories.DataColetaRepository;

@Service
public class DataColetaService {

	@Autowired
	private DataColetaRepository repository;
	
	private DataColeta dataColeta;
	private Map<String, DataColeta> dataColetas = new HashMap<>();
	
	public List<DataColeta> findAll() {
		return repository.findAll();
	}
	
	public DataColeta insert(DataColeta obj) {
		
		if(dataColetas.get(obj.getData().toString()) == null) {
			dataColeta = repository.save(obj);
			dataColetas.put(dataColeta.getData().toString(), dataColeta);
		}else {
			dataColeta = dataColetas.get(obj.getData().toString());
		}
		
		return dataColeta;
	}

}
