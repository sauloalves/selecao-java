package com.selecao.desafio.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecao.desafio.entities.Distribuidora;
import com.selecao.desafio.repositories.DistribuidoraRepository;

@Service
public class DistribuidoraService {

	@Autowired
	private DistribuidoraRepository repository;
	
	private Distribuidora distribuidora;
	private Map<String, Distribuidora> distribuidoras = new HashMap<>();
	
	public List<Distribuidora> findAll() {
		return repository.findAll();
	}
	
	public Distribuidora insert(Distribuidora obj) {
		
		if(distribuidoras.get(obj.getNome()) == null) {
			distribuidora = repository.save(obj);
			distribuidoras.put(distribuidora.getNome(), distribuidora);
		}else {
			distribuidora = distribuidoras.get(obj.getNome());
		}
		
		return distribuidora;
	}
}
