package com.selecao.desafio.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecao.desafio.dtos.HistoricoPrecoCombustivelDTO;
import com.selecao.desafio.entities.HistoricoPrecoCombustivel;
import com.selecao.desafio.repositories.HistoricoPrecoCombustivelRepository;

@Service
public class HistoricoPrecoCombustivelService {

	@Autowired
	private HistoricoPrecoCombustivelRepository repository;
	
	public List<HistoricoPrecoCombustivel> findAll() {
		return repository.findAll();
	}
	
	public HistoricoPrecoCombustivel insert(HistoricoPrecoCombustivel obj) {
		return repository.save(obj);
	}

	public HistoricoPrecoCombustivel findById(Long id) {
		Optional<HistoricoPrecoCombustivel> obj = repository.findById(id);
		return obj.get();
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	public HistoricoPrecoCombustivel update(Long id, HistoricoPrecoCombustivelDTO obj) {
		HistoricoPrecoCombustivel historico = repository.getOne(id);
		udateData(historico, obj);
		return repository.save(historico);
	}
	
	private void udateData(HistoricoPrecoCombustivel historico, HistoricoPrecoCombustivelDTO obj) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		historico.setCombustivel(obj.getCombustivel());
		historico.setPreco(obj.getPreco());
		try {
			historico.setData(sdf.parse(obj.getData()));
		} catch (ParseException e) {
			System.out.println("udateData - Erro no formato da data: " + e.getMessage());
		}
	}

	public List<HistoricoPrecoCombustivel> historico() {
		return repository.getHistorico();
	}

	public HistoricoPrecoCombustivel gravaHistorico(HistoricoPrecoCombustivelDTO obj) {
		HistoricoPrecoCombustivel hpc = new HistoricoPrecoCombustivel();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
		Date data;
		try {
			data = formato.parse(obj.getData());
			
			hpc = new HistoricoPrecoCombustivel(null, obj.getCombustivel(), data, obj.getPreco());
		} catch (ParseException e) {
			System.out.println("Erro no formato da data de nascimento: " + e.getMessage());
		}
		return insert(hpc);
	}

}
