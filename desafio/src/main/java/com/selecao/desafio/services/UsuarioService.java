package com.selecao.desafio.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.selecao.desafio.dtos.UsuarioDTO;
import com.selecao.desafio.entities.Usuario;
import com.selecao.desafio.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository repository;
	
	public List<Usuario> findAll() {
		return repository.findAll();
	}
	
	public Usuario insert(Usuario obj) {
		return repository.save(obj);
	}

	public Usuario findById(Long id) {
		Optional<Usuario> obj = repository.findById(id);
		return obj.get();
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}
	
	public Usuario update(Long id, UsuarioDTO obj) {
		Usuario usuario = repository.getOne(id);
		udateData(usuario, obj);
		return repository.save(usuario);
	}
	
	private void udateData(Usuario usuario, UsuarioDTO obj) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		usuario.setNome(obj.getNome());
		usuario.setSobrenome(obj.getSobrenome());
		usuario.setEmail(obj.getEmail());
		try {
			usuario.setDataNascimento(sdf.parse(obj.getDataNascimento()));
		} catch (ParseException e) {
			System.out.println("UpdateData - Erro no formato da data de nascimento: " + e.getMessage());
		}
	}

	public Usuario gravaUsuario(UsuarioDTO usuario) {
		Usuario user = new Usuario();
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
		Date dataNascimento;
		try {
			dataNascimento = formato.parse(usuario.getDataNascimento());
			
			user = new Usuario(null, usuario.getNome(), usuario.getSobrenome(), usuario.getEmail(),
					dataNascimento);
		} catch (ParseException e) {
			System.out.println("Erro no formato da data de nascimento: " + e.getMessage());
		}
		return insert(user);
	}
	
}
